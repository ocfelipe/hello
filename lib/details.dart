import 'package:flutter/material.dart';
import 'package:hello/worldpopulation.dart';

class DetailsCountry extends StatelessWidget {
  WorldPopulation _worldPopulation;
  DetailsCountry(this._worldPopulation);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Details")),
      body: buildDetails(),
    );
  }

  Widget buildDetails() {
    return Container(
        color: Color(0xFF555555),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 20.0, bottom: 30.0),
                  child: CircleAvatar(
                    radius: 40.0,
                    backgroundColor: Colors.blueAccent,
                    backgroundImage: NetworkImage("${_worldPopulation.flag}"),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 20.0),
                  child: Text(
                    "${_worldPopulation.country}",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 30.0),
                    )
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 20.0),
                  child: Text("Rank:" +
                    " ${_worldPopulation.rank}",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0),
                    )
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 20.0),
                  child: Text("Population:" +
                    " ${_worldPopulation.population}",
                    style: TextStyle(
                      color: Colors.yellowAccent,
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0),
                    )
                )
              ],
            )
          ],
        ));
  }
}
