import 'package:flutter/material.dart';
import 'package:hello/world_api.dart';
import 'package:hello/worldpopulation.dart';
import 'package:hello/details.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter',
        home: Scaffold(
          appBar: AppBar(title: Center(child: Text('Flutter INF1300'))),
          body: new WorldWidget(),
        ));
  }
}

class WorldWidget extends StatefulWidget {
  @override
  _WorldWidgetState createState() => _WorldWidgetState();
}

class _WorldWidgetState extends State<WorldWidget> {
  WorldApi _api = new WorldApi();
  List<WorldPopulation> _worldpopulation;

  @override
  void initState() {
    super.initState();
    getWorld();
  }

  @override
  Widget build(BuildContext context) {
    return _worldpopulation == null
        ? CircularProgressIndicator()
        : ListView.builder(
            itemCount: _worldpopulation.length,
            itemBuilder: (BuildContext context, int index) {
              WorldPopulation world = _worldpopulation[index];

              return GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) =>
                          new DetailsCountry(world)));
                },
                child: ListTile(
                  title: Text("${world.country}" + " - " + "${world.population}"),
                  leading: CircleAvatar(
                    backgroundColor: Colors.blueAccent,
                    backgroundImage: NetworkImage("${world.flag}"),
                  ),
                ),
              );
            });
  }

  void getWorld() async {
    _worldpopulation = await _api.loadJsonFromApi();
    setState(() {});
  }
}
